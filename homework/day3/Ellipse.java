package homework.day3;

public class Ellipse extends Figure implements Moveable {
    public Ellipse(int x, int y) {
        super(x, y);
    }

    @Override
    public double getPerimetr() {
        return (x + y) * 3.13;
    }

    @Override
    public void move(int x, int y) {
        this.x += x;
        this.y += y;
    }
}
