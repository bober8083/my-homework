package homework.day3;

public interface Moveable {
    void move(int x, int y);
}
