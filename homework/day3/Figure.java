package homework.day3;

public abstract class Figure {
    protected int x;
    protected int y;

    public Figure(int x, int y) {
        this.x = x;
        this.y = y;
    }

    abstract double getPerimetr();

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
