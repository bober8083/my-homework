package homework.calendars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GregorianTest {
    @Test
    public void test_1() {
        Gregorian a = new Gregorian(1, 2, 1999);
        assertNotNull(a);
    }

    @Test
    public void test_2() {
        Gregorian a = new Gregorian(1, 2, 1999);
        assertNotNull(a.getString());
    }

    @Test
    public void test_3() {
        Gregorian a = new Gregorian(1, 2, 1999);
        a.addDay();
        assertTrue(a.getDay() == 2);
        assertTrue(a.getMonth() == 2);
        assertTrue(a.getYear() == 1999);
    }

    @Test
    public void test_4() {
        Gregorian a = new Gregorian(31, 8, 2000);
        a.addDay();
        assertTrue(a.getDay() == 1);
        assertTrue(a.getMonth() == 9);
        assertTrue(a.getYear() == 2000);
    }

    @Test
    public void test_5() {
        Gregorian a = new Gregorian(29, 2, 2000);
        a.addDay();
        assertTrue(a.getDay() == 1);
        assertTrue(a.getMonth() == 3);
        assertTrue(a.getYear() == 2000);
    }

    @Test
    public void test_6() {
        Gregorian a = new Gregorian(28, 2, 2000);
        a.addDay();
        assertTrue(a.getDay() == 29);
        assertTrue(a.getMonth() == 2);
        assertTrue(a.getYear() == 2000);
    }

    @Test
    public void test_7() {
        Gregorian a = new Gregorian(31, 12, 2000);
        a.addDay();
        assertTrue(a.getDay() == 1);
        assertTrue(a.getMonth() == 1);
        assertTrue(a.getYear() == 2001);
    }

    @Test
    public void test_8() {
        Gregorian a = new Gregorian(30, 6, 2001);
        a.addDay();
        assertTrue(a.getDay() == 1);
        assertTrue(a.getMonth() == 7);
        assertTrue(a.getYear() == 2001);
    }

    @Test
    public void test_9() {
        Gregorian a = new Gregorian(31, 10, 2001);
        a.addDay();
        assertTrue(a.getDay() == 1);
        assertTrue(a.getMonth() == 11);
        assertTrue(a.getYear() == 2001);
    }

    @Test
    public void test_is_leap_1() {
        assertTrue(Gregorian.isLeapYear(2013));
        assertTrue(!Gregorian.isLeapYear(1999));
        assertTrue(!Gregorian.isLeapYear(1900));
        assertTrue(Gregorian.isLeapYear(1600));
        assertTrue(!Gregorian.isLeapYear(2022));
        assertTrue(!Gregorian.isLeapYear(1234));
        assertTrue(!Gregorian.isLeapYear(1234));
        assertTrue(Gregorian.isLeapYear(-2000));
        assertTrue(Gregorian.isLeapYear(0));
        assertTrue(!Gregorian.isLeapYear(-1234));
    }

    @Test
    public void test_is_leap_2() {
        for (int i = 0; i < 120; ++i) {
            assertEquals(Gregorian.isLeapYear(i), Gregorian.isLeapYear(-i));
        }
    }
}

