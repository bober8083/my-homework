package homework.calendars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class HidzraTest {
    @Test
    public void test_is_leap_1() {
        assertTrue(!Gregorian.isLeapYear(1435));
        assertTrue(Gregorian.isLeapYear(1436));
        assertTrue(!Gregorian.isLeapYear(1437));
        assertTrue(!Gregorian.isLeapYear(1438));
        assertTrue(!Gregorian.isLeapYear(1439));
        assertTrue(!Gregorian.isLeapYear(1439));
        assertTrue(Gregorian.isLeapYear(1440));
    }
}