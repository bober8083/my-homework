package homework.calendars;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ConverterTest {
    @Test
    public void test_convert_1() {
        Gregorian g = new Gregorian(1, 3, 2015);
        Hidzra h = Converter.gregorianToHidzra(g);
        Gregorian g1 = Converter.hidzraToGregorian(h);
        assertEquals(g, g1);
    }
}