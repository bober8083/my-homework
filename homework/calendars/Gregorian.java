package homework.calendars;

import java.util.Objects;

public class Gregorian implements Date {
    private static final int[] DAYS_IN_MONTH = {
            31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
    };

    private int day;
    private int month;
    private int year;

    public Gregorian(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;

        assert month >= 1 && month <= 12;
        assert day >= 1 && day <= 31;
        if (month != 2) {
            assert day <= DAYS_IN_MONTH[month - 1];
        } else {
            assert day <= DAYS_IN_MONTH[month - 1] +
                    (isLeapYear(year) ? 1 : 0);
        }
    }

    @Override
    public String getString() {
        return String.format("%d/%d/%d", year, month, day);
    }

    @Override
    public void addDay() {
        this.day += 1;
        if (day > getDaysInMonth(month, year)) {
            day = 1;
            month += 1;
        }
        if (month > 12) {
            month = 1;
            year += 1;
        }
    }

    private int getDaysInMonth(int month, int year) {
        int res = DAYS_IN_MONTH[month - 1];
        if (month == 2) {
            if (isLeapYear(year)) {
                res += 1;
            }
        }
        return res;
    }

    public static boolean isLeapYear(int inYear) {
        int year = Math.abs(inYear);

        if (year % 4 != 0) {
            return false;
        }
        if (year % 400 == 0) {
            return true;
        }
        if (year % 100 == 0) {
            return false;
        }
        return true;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gregorian gregorian = (Gregorian) o;
        return day == gregorian.day && month == gregorian.month && year == gregorian.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, month, year);
    }
}
