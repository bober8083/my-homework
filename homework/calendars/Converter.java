package homework.calendars;

public class Converter {
    public static Hidzra gregorianToHidzra(Gregorian inParam) {
        // naive implementation o(n)
        //5 ноября 2013 года - 1 мухаррам 1435
        Gregorian initDate = new Gregorian(5, 11, 2013);
        Hidzra res = new Hidzra(1, 1, 1435);
        while (!initDate.equals(inParam)) {
            initDate.addDay();
            res.addDay();
        }
        return res;
    }

    public static Gregorian hidzraToGregorian(Hidzra inParam) {
        // naive implementation o(n)
        //5 ноября 2013 года - 1 мухаррам 1435
        Gregorian res = new Gregorian(5, 11, 2013);
        Hidzra initDate = new Hidzra(1, 1, 1435);

        while (!initDate.equals(inParam)) {
            initDate.addDay();
            res.addDay();
        }

        return res;
    }
}
