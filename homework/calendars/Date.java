package homework.calendars;

public interface Date {
    String getString();
    void addDay();
}
