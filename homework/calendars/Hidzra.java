package homework.calendars;

import java.util.Objects;

public class Hidzra implements Date {
    // naive implementation
    private static final int[] DAYS_IN_MONTH = {
            30, 29, 30, 29, 30, 29, 30, 29, 30, 29, 30, 29
    };

    private int day;
    private int month;
    private int year;

    public Hidzra(int day, int month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;

        assert month >= 1 && month <= 12;
        assert day >= 1 && day <= 31;
    }

    @Override
    public String getString() {
        return String.format("%d/%d/%d", year, month, day);
    }

    @Override
    public void addDay() {
        this.day += 1;
        if (day > getDaysInMonth(month, year)) {
            day = 1;
            month += 1;
        }
        if (month > 12) {
            month = 1;
            year += 1;
        }
    }

    private int getDaysInMonth(int month, int year) {
        int res = DAYS_IN_MONTH[month - 1];
        if (month == 12) {
            if (isLeapYear(year)) {
                res += 1;
            }
        }
        return res;
    }

    public static boolean isLeapYear(int year) {
        // turkish style of leap year detection
        int yearInCycle = year % 8;
        return yearInCycle == 2 || yearInCycle == 5 || yearInCycle == 7;
    }

    public int getDay() {
        return day;
    }

    public int getMonth() {
        return month;
    }

    public int getYear() {
        return year;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hidzra hidzra = (Hidzra) o;
        return day == hidzra.day && month == hidzra.month && year == hidzra.year;
    }

    @Override
    public int hashCode() {
        return Objects.hash(day, month, year);
    }
}
