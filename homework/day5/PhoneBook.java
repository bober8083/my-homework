package homework.day5;

public interface PhoneBook {
    void addNumber(String name, String phone);
    String findPhoneByName(String name);
}
