package homework.day5;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestLambda {
    public static void main(String[] args) throws Throwable {
        ForkJoinPool.commonPool().submit(() -> {
            System.out.println("test lambda!");
        });

        final int bzz = 1998;

        Summable s1 = new Summable() {
            @Override
            public int sum(int a, int b) {
                return a + b;
            }
        };

        Summable s2 = (a, b) -> {
            if (a > 1) {
                return (a + b + bzz);
            } else {
                return (a + b - bzz);
            }
        };

        List<Integer> a1 = Stream.of(1, 23, 3, 4)
                .collect(Collectors.toList());

        List<Integer> a2 = a1.stream().filter(f -> f != null)
                .filter(s -> s > 3)
                .collect(Collectors.toList());

        a2.stream().forEach(f -> System.out.println(f));

        //System.out.println(s1.sum(1, 2));
        int b = s2.sum(2, 3);
        System.out.println(b);

        System.out.println("Test 1");
        System.out.println("Test 2");

        TestVoid test2 = () -> (new Date()).getYear();
        System.out.println(test2.testMe());

//        Thread.sleep(5000);
//        System.out.println("Test 3");

        Map<String, String> wordBookEn = new HashMap<>();
        Map<String, String> wordBookRu = new HashMap<>();

        wordBookEn.put("run", "бежать");
        wordBookEn.put("kill", "убивать");
        wordBookEn.put("delete", "удалять");

        wordBookRu.put("бежать", "run");
        wordBookRu.put("убивать", "kill");
        wordBookRu.put("удалять", "delete");

        System.out.println(wordBookEn.get("kill"));
        System.out.println(wordBookEn.get("destroy"));

        System.out.println(wordBookRu.get("бежать"));

        Map<Integer, List<String>> events = new HashMap<>();
        events.put(1998, Stream.of("Default", "Chubais")
                .collect(Collectors.toList()));
        events.put(2001, Stream.of("Millenium")
                .collect(Collectors.toList()));

        System.out.println(events);

        Runnable runnable1 = () -> System.out.println("Inside runnable interface!");
        runnable1.run();

        Callable<Integer> callable1 = () -> {
            System.out.println("Inside callable interface!");
            return 123;
        };
        System.out.println("From callable returned" + callable1.call());
        ForkJoinPool.commonPool().submit(runnable1);

        ByCondition byCondition = generateByCondition();

        int[] arr = {1, 3, 65, 324, 235, 2, 12, 6576};
        int total1 = 0;

        for (int i = 0; i < arr.length; ++i) {
            if (byCondition.isOk(arr[i])) {
                ++total1;
            }
        }

        int[] newArr = new int[total1];
        int total2 = 0;

        Predicate<Integer> byCondLib = (t) -> t % 3 == 0;
        for (int i = 0; i < arr.length; ++i) {
            if (byCondLib.test(arr[i])) {
                ++total2;
            }
        }

        assert total2 == total1;

        Random rnd = new Random(20000);
        int rndArr[] = {rnd.nextInt(1000),
                rnd.nextInt(1000),
                rnd.nextInt(1000)};

        assert total2 == total1;

        int res1 = sumArgs(rndArr);
        System.out.println(res1);

        int res2 = findMinElementIndex(3, 1, 2, 3);
        System.out.println(res2);
    }

    public static ByCondition generateByCondition() {
        return (param) -> (param % 3) == 0;
    }

    private static int sumArgs(int... param) {
        int res = 0;
        if (param == null) {
            return res;
        }

        for (int i = 0; i < param.length; ++i) {
            res = res + param[i];
        }

        return res;
    }

    private static int minusArgs(int... param) {
        if (param == null || param.length == 0) {
            return 0;
        }

        int minElementIndex = findMinElementIndex(param);
        int res = param[minElementIndex];

        for (int i = 0; i < param.length; ++i) {
            if (minElementIndex == i) {
                continue;
            }
            res = res - param[i];
        }

        return res;
    }

    private static int findMinElementIndex(int... param) {
        if (param == null || param.length == 0) {
            return -1;
        }

        int res = 0;
        int minValue = param[res];

        for (int i = 1; i < param.length; ++i) {
            if (param[i] < minValue) {
                res = i;
                minValue = param[i];
            }
        }

        return res;
    }
}