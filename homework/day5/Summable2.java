package homework.day5;

public interface Summable2 {
    int sum1(int a, int b);
    int sum2(int a, int b, int c);
    void loadFromDatabase();
}
