package homework.day5;

import java.util.function.Predicate;

public class FilterImpl<T> {
    public T[] filterGeneric(T[] inParam,
                             Predicate<T> predicate) {
        if (inParam == null || inParam.length == 0) {
            return inParam;
        }

        int newLen = 0;
        for (int i = 0; i < inParam.length; ++i) {
            if (predicate.test(inParam[i])) {
                ++newLen;
            }
        }

        T[] r = (T[])java.lang.reflect.Array.newInstance(
                inParam.getClass().getComponentType(),
                newLen);

        int curr = 0;
        for (int i = 0; i < inParam.length; ++i) {
            if (predicate.test(inParam[i])) {
                r[curr] = inParam[i];
                curr++;
            }
        }

        return r;
    }

    public static void main(String[] args) {
        String[] a = {"123", "234"};

        FilterImpl impl = new FilterImpl<String>();
        String[] t55 = (String[]) impl.filterGeneric(a,
                (s) -> checkIfTooLong((String)s));
        System.out.println(t55);

        Integer[] a2 = {-3, 1, 2, 3, 4};
        FilterImpl impl2 = new FilterImpl<Integer>();
        Integer[] t56 = (Integer[]) impl2.filterGeneric(a2,
                (s) -> checkIfTooLong((Integer)s));
        System.out.println(t56);

        System.out.println("the end!");
    }

    public static boolean checkIfTooLong(String param) {
        return param.length() > 2;
    }

    public static boolean checkIfTooLong(Integer param) {
        return param > 2;
    }

    public static boolean testInternal(String t) {
        return true;
    }
}
