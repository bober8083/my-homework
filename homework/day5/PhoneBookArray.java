package homework.day5;

public class PhoneBookArray implements PhoneBook {
    private String[] names = new String[100];
    private String[] numbers = new String[100];
    private int curr = 0;

    public PhoneBookArray() {
    }

    @Override
    public void addNumber(String name, String phone) {
        names[curr] = name;
        numbers[curr] = phone;
        ++curr;
    }

    @Override
    public String findPhoneByName(String name) {
        for (int i = 0; i < curr; ++i) {
            if (names[curr].equals(name)) {
                return numbers[curr];
            }
        }
        return null;
    }
}
