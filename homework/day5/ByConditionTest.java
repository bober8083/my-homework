package homework.day5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ByConditionTest {
    @Test
    public void test_1() {
        ByCondition byCond = TestLambda.generateByCondition();
        assert !byCond.isOk(4);
        assert byCond.isOk(3);
    }
}