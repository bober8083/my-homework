package homework.day5;

@FunctionalInterface
public interface Summable {
    int sum(int a, int b);
}
