package homework.day5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class FileReader {
    public static final Charset UTF8_CHARSET = StandardCharsets.UTF_8;

    public static String readFile(String fileName) {
        File f = new File(fileName);
        StringBuilder res = new StringBuilder();

        try (InputStream is = new FileInputStream(f)) {
            try (InputStreamReader streamReader = new InputStreamReader(is, UTF8_CHARSET)) {
                try (BufferedReader br = new BufferedReader(streamReader)) {
                    String line;
                    while ((line = br.readLine()) != null) {
                        res.append(line);
                        res.append("\n");
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return res.toString();
    }
}
