package homework.day5;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class PhoneBookMap implements PhoneBook {
    private Map<String, String> nums = new HashMap<String, String>();
    //private Map<String, String> nums = new TreeMap<>();

    public PhoneBookMap() {

    }

    @Override
    public void addNumber(String name, String phone) {
        nums.put(name, phone);
    }

    @Override
    public String findPhoneByName(String name) {
        return nums.get(name);
    }
}