package homework.day5;

public class AnyMethodAsLambda {
    public static void main(String[] args) {
        AnyMethodAsLambda theApp = new AnyMethodAsLambda();
        theApp.run();
    }

    void run() {
        MyMegaInterface f1 = (a, b) -> getStringWrapper(a, b);
        System.out.println(f1.getString(2, 4));
        System.out.println(f1.getString(12, 33));

        ByCondition f2 = (a) -> isOkInternal(a);
        System.out.println(f2.isOk(3));
        System.out.println(f2.isOk(4));
        System.out.println(f2.isOk(5));
    }

    private String getStringWrapper(int a, int b) {
        return (new Integer(a + b)).toString();
    }

    // просто демонстрационная функция, считает все числа от одного до param
    // в заданях таких не было, будьте осторожны
    private boolean isOkInternal(int param) {
        int res = 0;
        for (int i = 1; i < param; ++i) {
            res += i;
        }
        return (res % 3 == 0);
    }
}
