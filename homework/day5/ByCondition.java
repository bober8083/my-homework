package homework.day5;

@FunctionalInterface
public interface ByCondition {
    boolean isOk(int param);
}
