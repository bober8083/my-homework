package homework.day10;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main(String[] args) {
        Random r = new Random(10);

        List<Integer> s = new ArrayList<>();
        for (int i = 0; i < 100; ++i) {
            s.add(i);
        }

//        s = s.stream().sorted().collect(Collectors.toList());
//        s = (s.stream())
//                .filter(f -> f % 2 == 0)
//                .filter(f -> f > 10 && f < 100)
//                .collect(Collectors.toList());

        List<String> res =
                s.stream().map(f -> String.valueOf(f))
                        .collect(Collectors.toList());

        System.out.println("");

        List<Car> r1 = new ArrayList<>();
        r1.add(new Car());
        r1.get(0).model = "lada";
        r1.get(0).owner = "denys";

        r1.add(new Car());
        r1.get(1).model = "oka";
        r1.get(1).owner = "denys";

        r1.add(new Car());
        r1.get(2).model = "oka";
        r1.get(2).owner = "denys";

        List<Car> c = r1.stream()
                .distinct()
                .collect(Collectors.toList());
        System.out.println("");

        Stream<Integer> s1 = s.stream().filter(f -> f % 2 == 0).sorted();
        //List<Integer> a1 = s1.collect(Collectors.toList());
        Stream<Integer> s2 = s.stream().filter(f -> f % 2 != 0).sorted();
        //List<Integer> a2 = s2.collect(Collectors.toList());

        Stream.concat(s1, s2).forEach(f -> System.out.println(f));
        System.out.println("");


//        List<Integer> s = new ArrayList<>();
//        for (int i = 0; i < 100_000_000; ++i) {
//            s.add(i + 19);
//        }
//
//        long from = System.currentTimeMillis();
//        s.stream().filter(f -> f % 2 == 0)
//                .collect(Collectors.toList());
//        long to = System.currentTimeMillis();
//
//        s.stream()
//                .parallel()
//                .filter(f -> f % 2 == 0)
//                .collect(Collectors.toList());
//        long to2 = System.currentTimeMillis();
//
//        System.out.println(to - from + " ms");
//        System.out.println(to2 - to + " ms");
//        System.out.println("");
    }

    Stream<Integer> getStrem() {
        return Stream.of(12, 1213, 353, 325);
    }

    void getStrem(Stream<Integer> param) {
        param.sorted().collect(Collectors.toList());
    }
}
