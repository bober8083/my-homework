package homework.day10;

import java.util.Objects;

public class Car {
    public String model;
    public String owner;

    Car() {
        model = "";
        owner = "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(model, car.model) && Objects.equals(owner, car.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(model, owner);
    }
}
