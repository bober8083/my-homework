package homework.day7;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

// -Xmx2048m -Xms512m
public class TestArrays {
    public static void main(String[] arg) {
        int[] a = {1, 2, 3};

        List<Integer> arr1 = new ArrayList<>();
        arr1.add(1);
        arr1.add(3);
        arr1.add(4);
        arr1.add(2, 8);

        System.out.println(arr1);

        Integer[] a2 = toArray(arr1);

        Iterator<Integer> it1 = arr1.iterator();
        while (it1.hasNext()) {
            Integer i = it1.next();
            System.out.println(i);
        }

        ListIterator<Integer> it = arr1.listIterator();
        while (it.hasNext()) {
            Integer i = it.next();
            it.add(2022);
            System.out.println(i);
        }

        System.out.println(arr1.contains(2022));
        System.out.println(arr1.contains("2022"));

        for (Integer curr : arr1) {
            System.out.println(curr);
        }

        Long nin = new Long(884364334634634L);
        int fail = nin.intValue();

        //можно поиграться с разными настройками intellij idea
        // JVM, и т.п. и классом DummyClass

        //testArrayList_Integer();
        //testLinkedList_Integer();

        //testArrayList_Dummy();
        //testLinkedList_Dummy();

        //testArrayList_Dummy_2();
        //testLinkedList_Dummy_2();

        //checkContains();
        checkContains_HashSet();

        Object o = new String("123");

        Set<DummyClass> s = new HashSet<>();
        s.add(new DummyClass());
        s.add(new DummyClass());
    }

    static void checkContains() {
        int fromNum = 100000;
        List<Integer> k = generateLongArray(fromNum);

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; ++i) {
            boolean cont = k.contains(fromNum + i);
            //boolean cont = k.contains(i);
        }

        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Discovered for " + duration + "ms");
    }

    static void checkContains_HashSet() {
        int fromNum = 100000;
        Set<Integer> k = new HashSet<>(generateLongArray(fromNum));

        long start = System.currentTimeMillis();
        for (int i = 0; i < 1000; ++i) {
            boolean cont = k.contains(fromNum + i);
            //boolean cont = k.contains(i);
        }

        long end = System.currentTimeMillis();
        long duration = end - start;
        System.out.println("Discovered for " + duration + "ms");
    }

    private static List<Integer> generateLongArray(int max) {
        List<Integer> res = new ArrayList<>();
        for (int i = 0; i < max; ++i) {
            for (int j = 0; j < 5; ++j) {
                res.add(i + j);
            }
        }
        return res;
    }

    static void testArrayList_Integer() {
        List<Integer> arr2 = new ArrayList<>();

        List<Integer> batch = new ArrayList<>();
        for (int i = 0; i < 2000000; ++i) {
            batch.add(i);
        }

        int cnt = 0;
        while (true) {
            arr2.addAll(batch);
            System.err.println("Next turn " + cnt);
            ++cnt;
        }
    }

    static void testLinkedList_Integer() {
        List<Integer> arr2 = new LinkedList<>();

        List<Integer> batch = new ArrayList<>();
        for (int i = 0; i < 2000000; ++i) {
            batch.add(i);
        }

        int cnt = 0;
        while (true) {
            arr2.addAll(batch);
            System.err.println("Next turn " + cnt);
            ++cnt;
        }
    }

    static void testArrayList_Dummy() {
        List<DummyClass> arr2 = new ArrayList<>();

        List<DummyClass> batch = new ArrayList<>();
        for (int i = 0; i < 2000000; ++i) {
            batch.add(new DummyClass());
        }

        int cnt = 0;
        while (true) {
            arr2.addAll(batch);
            System.err.println("Next turn " + cnt);
            ++cnt;
        }
    }

    static void testLinkedList_Dummy() {
        List<DummyClass> arr2 = new LinkedList<>();

        List<DummyClass> batch = new ArrayList<>();
        for (int i = 0; i < 2000000; ++i) {
            batch.add(new DummyClass());
        }

        int cnt = 0;
        while (true) {
            arr2.addAll(batch);
            System.err.println("Next turn " + cnt);
            ++cnt;
        }
    }

    static void testArrayList_Dummy_2() {
        List<DummyClass> arr2 = new ArrayList<>();

        List<DummyClass> batch = new LinkedList<>();
        for (int i = 0; i < 2000000; ++i) {
            batch.add(new DummyClass());
        }

        int cnt = 0;
        while (true) {
            arr2.addAll(batch);
            System.err.println("Next turn " + cnt);
            ++cnt;
        }
    }

    static void testLinkedList_Dummy_2() {
        List<DummyClass> arr2 = new LinkedList<>();

        List<DummyClass> batch = new LinkedList<>();
        for (int i = 0; i < 2000000; ++i) {
            batch.add(new DummyClass());
        }

        int cnt = 0;
        while (true) {
            arr2.addAll(batch);
            System.err.println("Next turn " + cnt);
            ++cnt;
        }
    }

    public String get() {
        return "2133";
    }

    public static Integer[] toArray(List<Integer> al)
    {
        Integer[] arr = new Integer[al.size()];
        arr = al.toArray(arr);
        return arr;
    }

    public static List<Integer> toList(Integer[] al)
    {
        return new ArrayList<Integer>(Arrays.asList(al));
    }
}

class DummyClass {
    private static final Random RANDOM = new Random(1213);

    private String s1;
    private Integer i1;

    private String s2;
    private Long i2;

    DummyClass() {
        i1 = RANDOM.nextInt();
        s1 = ((Integer)(RANDOM.nextInt())).toString();
        s2 = ((Integer)(RANDOM.nextInt())).toString();
        i2 = RANDOM.nextLong();
    }

    public String getS() {
        return s1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DummyClass that = (DummyClass) o;
        return Objects.equals(s1, that.s1)
                && Objects.equals(i1, that.i1)
                && Objects.equals(s2, that.s2)
                && Objects.equals(i2, that.i2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(s1, i1, s2, i2);
    }
}
