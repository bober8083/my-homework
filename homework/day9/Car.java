package homework.day9;

public class Car {
    public Integer id;
    public String model;
    public Integer year;

    public static String carToString(Car s) {
        return String.format("%d|%s|%d",
                s.id, s.model, s.year);
    }

    public static Car parseFromString(String s) {
        Car res = new Car();
        String[] parts = s.split("\\|");
        if (parts == null || parts.length != 3) {
            throw new RuntimeException("Wrong file format");
        }

        res.id = Integer.valueOf(parts[0]);
        res.model = parts[1];
        res.year = Integer.valueOf(parts[2]);

        return res;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", model='" + model + '\'' +
                ", year=" + year +
                '}';
    }
}
