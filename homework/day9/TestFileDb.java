package homework.day9;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class TestFileDb {
    private static final String FILE_NAME = "d:/cars.txt";
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        TestFileDb app = new TestFileDb();
        app.run();
    }

    private void run() {
        DatabaseState state = new DatabaseState();

        List<String> initialText = readAllAsText(FILE_NAME);

        for (int i = 0; i < initialText.size(); ++i) {
            state.add(Car.parseFromString(initialText.get(i)));
        }

        addRandomCars(state);
        Car c = state.findById(2);
        System.out.println(c);

        Car c2 = null;
        System.out.println(c2);



        List<String> endLines = objectsToString(state.toList());
        writeAllAsText(FILE_NAME, endLines);
    }

    private void addRandomCars(DatabaseState state) {
        for (int i = 0; i < 50000; ++i) {
            Car n = new Car();
            n.id = state.maxId() + 1;
            n.model = "lada";
            n.year = 1990 + RANDOM.nextInt(30);
            state.add(n);
        }
    }

    private List<String> objectsToString(List<Car> inialObjects) {
        List<String> res = new ArrayList<>();
        for (int i = 0; i < inialObjects.size(); ++i) {
            res.add(Car.carToString(inialObjects.get(i)));
        }
        return res;
    }

    public List<String> readAllAsText(String fileName) {
        File text = new File(fileName);
        List<String> res = new ArrayList<>();

        //Creating Scanner instance to read File in Java
        try (Scanner scnr = new Scanner(text)) {
            //Reading each line of the file using Scanner class
            while (scnr.hasNextLine()) {
                String line = scnr.nextLine();
                res.add(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }

        return res;
    }

    public void writeAllAsText(String fileName, List<String> lines) {
        try (FileWriter fstream = new FileWriter(fileName)) {
            try (BufferedWriter out = new BufferedWriter(fstream)) {
                for (int i = 0; i < lines.size(); ++i) {
                    out.write(lines.get(i));
                    out.write("\n");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
