package homework.day9;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DatabaseState {
    public Map<Integer, Car> carMap = new HashMap<>();
    public Map<Car, Integer> s = new TreeMap<>();
    public Integer maxIdVal;

    public void add(Car c) {
        carMap.putIfAbsent(c.id, c);
    }

    public Car findById(Integer id) {
        return carMap.get(id);
    }

    public List<Car> toList() {
        List<Car> res = new ArrayList<>(carMap.values());
        return res;
    }

    public Integer maxId() {
        return maxIdVal;
    }
}
