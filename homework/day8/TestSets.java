package homework.day8;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.Set;

public class TestSets {
    public static void main(String[] args) {
        Set<KeyBucket> n = new HashSet<>();

        int sz = 100000;
        for (int i = 0; i < sz; ++i) {
            KeyBucket key1 = new KeyBucket(i * sz);
            KeyBucket key2 = new KeyBucket(i * 2 + sz);
            KeyBucket key3 = new KeyBucket(i * 3 + sz);

            n.add(key1);
            n.add(key2);
            n.add(key3);

            boolean cont = n.contains(key2);
            if (!cont) {
                System.out.println("test");
            }

            boolean cont1 = n.contains(key2);
            if (!cont) {
                System.out.println("test");
            }
        }

        Set<Integer> n1 = new HashSet<>();
        n1.add(1220);
        n1.add(1223);
        n1.add(1224);

        Set<Integer> n2 = new HashSet<>();
        n2.add(1225);
        n2.add(1224);
        n2.add(1223);

        for (Integer c : n2) {
            if (n1.contains(c)) {
                //
            }
        }

        Set<Integer> nres = new HashSet<>(n2);
        Set<Integer> k = Collections.unmodifiableSet(nres);

        //k.retainAll(n1);
        System.out.println(n);

        //check(null);
        test_stack_overflow(1);

        Map<Map<String, String>, String> p = new HashMap<>();
        p.hashCode();

        Map<Car, String> cars = new HashMap<>();
        cars.put(new Car("123-111"), "Ivan");
        cars.put(new Car("123-112"), "Sergey");
        cars.put(new Car("123-114"), "Denys");

        for (Map.Entry<Car, String> currCar : cars.entrySet()) {
            System.out.println(currCar.getKey().getMarka());
        }

        cars.putIfAbsent(new Car("123-114"), "Test");
        System.out.println("");

        // wrong iteration!! do not do it!!
        // could be too slow!
        for (Car currCar : cars.keySet()) {
            String owner = cars.get(currCar);
            System.out.println(owner);
        }
    }

    private static void test_stack_overflow(int p) {
        //test_stack_overflow(p);
    }

    private static void check(KeyBucket keyBucket) {
        System.out.println(keyBucket.toString());
    }
}

class KeyBucket {
    private static final Random RND = new Random();

    // calling of setters for params that included into hash function
    // is very bad idea. Not VERY VERY bad but effects will be also strange
    public void setParam(int param) {
        this.param = param;
    }

    private int param;

    KeyBucket(int param) {
        this.param = param;
    }

    public int getParam() {
        return param;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        KeyBucket keyBucket = (KeyBucket) o;
        return param == keyBucket.param;
    }

    @Override
    public int hashCode() {
        // return 42;  - VERY VERY BAD IDEA!
        // RND.nextInt(); -- VERY VERY BAD IDEA!
        return Objects.hashCode(param);
    }
}

class Car {
    final String gosNumber;

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    String marka;
    String model;

    public Car(String gosNumber) {
        this.gosNumber = gosNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return Objects.equals(gosNumber, car.gosNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(gosNumber);
    }
}
