package homework.day8;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class WordsCounter {
    public static void main(String[] args) throws WordsException {
        WordsCounter app = new WordsCounter();
        app.run();
    }

    private void run() {
        String fileName = "d:/baskervilles.txt";
        String allText = readAllAsText(fileName);
        process(allText);
    }

    private String readAllAsText(String fileName) throws WordsException {
        File text = new File(fileName);
        StringBuilder bld = new StringBuilder();

        //Creating Scanner instance to read File in Java
        Scanner scnr = null;
        try {
            scnr = new Scanner(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return "";
        }

        //Reading each line of the file using Scanner class
        while (scnr.hasNextLine()) {
            String line = scnr.nextLine();
            bld.append(line);
            bld.append(" ");
        }

        scnr.close();
        return bld.toString();
    }

    private List<String> readAllLines(String fileName) {
        File text = new File(fileName);
        List<String> res = new ArrayList<>();

        //Creating Scanner instance to read File in Java
        Scanner scnr = null;
        try {
            scnr = new Scanner(text);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }

        //Reading each line of the file using Scanner class
        while (scnr.hasNextLine()) {
            String line = scnr.nextLine();
            res.add(line);
        }

        scnr.close();
        return res;
    }

    private void process(String param) {
        String cleanString = preProcess(param);
        String[] words = cleanString.split(" ");
        Set<String> uniqWords = new HashSet<>();
        Map<String, Integer> frequency = new HashMap<>();

        for (String currWord : words) {
            String keyWord = currWord.toLowerCase();
            uniqWords.add(keyWord);
            frequency.putIfAbsent(keyWord, 0);
            Integer currFreq = frequency.get(keyWord);
            frequency.put(keyWord, currFreq + 1);
        }

        String answer = "";
        Integer anwFreq = 0;

        for (Map.Entry<String, Integer> curr : frequency.entrySet()) {
            if (curr.getValue() > anwFreq) {
                answer = curr.getKey();
                anwFreq = curr.getValue();
            }
        }

        System.out.println("Most frequency word: " + answer);
    }

    private String preProcess(String param) {
        StringBuilder builder = new StringBuilder();
        char[] k = param.toCharArray();

        for (int i = 0; i < k.length; ++i) {
            char t = k[i];
            if (t == '.' || t == ';' || t == '?' || t == ',' || t == '"' || t == '!') {
                continue;
            }

            if (t == ' ' && i < k.length - 1 && k[i + 1] == ' ') {
                continue;
            }

            builder.append(t);
        }

        return builder.toString();
    }
}
