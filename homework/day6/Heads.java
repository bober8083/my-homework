package homework.day6;

public interface Heads {
    String voice();
}

class MonkeyHead implements Heads {
    int size;

    public MonkeyHead(int size) {

    }

    public String voice() {
        return "cry";
    }
}

class FishHead implements Heads {
    int size;

    public FishHead(int size) {

    }

    public String voice() {
        return ">>-->>";
    }
}

class CatHead implements Heads {
    public CatHead() {

    }

    public String voice() {
        return "meow meow";
    }
}

class AnyHead {
    public String voice() {
        return "meowMeow";
    }
}
