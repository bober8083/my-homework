package homework.day6;

public class Animal<H extends Heads, B extends Bodies> {
    public H head;
    public B body;

    int age;
    int size;

    Animal(int age, int size) {
        this.age = age;
        this.size = size;
    }

    Animal(int age, int size, H head, B body) {
        this.age = age;
        this.size = size;
        this.head = head;
        this.body = body;
    }

    public void voice() {
        this.head.voice();
    }
}
