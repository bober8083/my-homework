package homework.day6;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class GenericMain {
    public static void main(String[] args) {
        Animal animal = new Animal<CatHead, DolphinBody>(2, 20);
        Animal<CatHead, DolphinBody> animal2 = new Animal<>(3, 330,
                new CatHead(), new DolphinBody());

        animal.head = new CatHead();
        animal.body = new DolphinBody();
        animal.voice();
        animal2.voice();

        List lst = new ArrayList();
        lst.add("test me!");
        lst.add(1);
        Object s = lst.get(0);
        String str1 = (String)s;
        //String str2 = (String)(lst.get(1));

        System.out.println(str1);
        //System.out.println(str2);

        List<String> arr = new ArrayList<String>();
        arr.add("say goodbye");
        arr.add("coool!");
        String s5 = arr.get(0);

        List<Integer> arrInt = new ArrayList<Integer>();
        arrInt.add(1);
        arrInt.add(2);

        Map<String, String> p = new TreeMap<>();
        p.put("go", "идти");
        p.put("run", "бежать");

        System.out.println(p.get("run"));
        System.out.println(p.get(1));

        AnimalOld a = new AnimalOld();
        a.head = new MonkeyHead(1);
        a.body = new DolphinBody();
    }
}
