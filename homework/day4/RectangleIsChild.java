package homework.day4;

class RectangleIsChild extends SquareIsParent {
    private int height;

    public RectangleIsChild(int width, int height) {
        super(width);
        this.height = height;

        assert width > 0;
        assert height > 0;
    }

    @Override
    public int getHeight() {
        return height;
    }
}
