package homework.day4;

public class TestApp {
    public static void main(String[] args) {
        RectangleIsParent rcp = new SquareIsChild(3);
        System.out.println("x:" + rcp.getWidth() + "  y:" + rcp.getHeight());

        SquareIsParent sqp = new RectangleIsChild(1, 3);
        printSquare(sqp);

        ClassWithCounter cnt1 = ClassWithCounter.create(true);
        ClassWithCounter cnt2 = ClassWithCounter.create(false);
        ClassWithCounter cnt3 = ClassWithCounter.create(true);
        ClassWithCounter cnt4 = ClassWithCounter.create(false);

        System.out.println("We created several insances of ClassWithCounter: " + ClassWithCounter.howMachOfInstances());
        System.out.println("the end!");

        int[] arr = {1, 2, 3, 4};
        calcMeArr(arr);

        calcMe(1, 2, 3, 4, 45);

        Car c = new Car();
        c.mileage = 250000;

        // create instance of inner class
        Car.Transmission transmission = c.new Transmission();

        c.year = 2013;

        System.out.println("test");

        Moveable m = new Moveable() {
            @Override
            public void move(int x, int y) {
                c.year += x;
                c.mileage -= y;
            }
        };

        m.move(1, 2);

        System.out.println("test");
    }

    private static void printSquare(SquareIsParent square) {
        System.out.println("square - x: " + square.getWidth() +
                "  y:" + square.getHeight());
    }

    static void calcMeArr(int[] param) {

    }

    static void calcMe(int... param) {
        class TestClass {
            int a;
            TestClass(int a) {
                this.a = a;
            }
        };

        return;
    }
}
