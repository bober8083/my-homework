package homework.day4;

public class Car {
    String brend;
    public int year;
    public int mileage;
    Transmission transmission1;
    Transmission transmission2;

    public Car() {
        this.transmission1 = new Transmission();
        this.transmission2 = new Transmission();
    }

    class Transmission {
        String vinCode;
        String serialNumber;
        String oilType;

        Transmission() {
            System.out.println("test");
        }
    }
}
