package homework.day4;

public abstract class SquareIsParent {
    protected int width;

    public SquareIsParent(int width) {
        this.width = width;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    abstract public int getHeight();
}
