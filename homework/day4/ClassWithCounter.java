package homework.day4;

class ClassWithCounter {
    private static int counter = 0;

    private static ClassWithCounter t = new ClassWithCounter(true);
    private static ClassWithCounter f = new ClassWithCounter(false);

    Boolean param;

    private ClassWithCounter(boolean param) {
        this.param = param;
        ++counter;
    }

    public static ClassWithCounter create(boolean p1) {
        if (p1) {
            return t;
        }
        return f;
    }

    public static int howMachOfInstances() {
        return counter;
    }
}
