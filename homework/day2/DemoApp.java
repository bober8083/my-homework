package homework.day2;

import java.util.Date;
import java.util.Random;

public class DemoApp {
    private static final Random RANDOM = new Random();

    public static void main(String[] args) {
        DemoApp theApp = new DemoApp();
        theApp.run();
    }

    public void run() {
        //main appliaction startup point
        System.out.println("20 - is odd " + checkIfOdd(20));
        System.out.println("23 - is odd " + checkIfOdd(23));

        java.util.Date dt = new Date();
        System.out.println(dt.getYear());

        java.time.LocalDate dt1 = java.time.LocalDate.now();
        System.out.println(dt1.getYear());

        for (int i = 0; i < 20; ++i) {
            System.out.println(RANDOM.nextInt(100));
        }
    }

    public boolean checkIfOdd(int param) {
        return param % 2 != 0;
    }
}
